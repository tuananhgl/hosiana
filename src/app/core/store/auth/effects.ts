import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { tap, map, switchMap, catchError } from 'rxjs/operators';

import * as fromActions from '@core/store/auth/actions';
import { AuthService } from '@core/services/auth.service';
import { LocalStorageService } from 'ngx-webstorage';

@Injectable({
  providedIn: 'root'
})
export class AuthEffects {
  constructor(
    private actions: Actions,
    private authService: AuthService,
    private localSt: LocalStorageService,
    private router: Router
  ) {}

  @Effect()
  logIn: Observable<any> = this.actions.ofType(fromActions.AuthActionTypes.LOGIN).pipe(
    map((action: fromActions.LogIn) => action.payload),
    switchMap(payload => {
      return this.authService.logIn(payload.email, payload.password).pipe(
        map(user => new fromActions.LogInSuccess({ user })),
        catchError(error => {
          return of(new fromActions.LogInFailure({ error: error }));
        })
      );
    })
  );

  @Effect({ dispatch: false }) // no emit something
  logInSuccess: Observable<Action> = this.actions.pipe(
    ofType(fromActions.AuthActionTypes.LOGIN_SUCCESS),
    tap((action: any) => {
      let now = Date.now();
      const data = Object.assign({}, action.payload.user.result.data, { created: now });
      // data.timeToLive = 5;
      this.localSt.store(fromActions.AUTH_KEY, data);
      this.router.navigateByUrl('/');
    })
  );

  @Effect({ dispatch: false })
  logInFailure: Observable<Action> = this.actions.pipe(
    ofType(fromActions.AuthActionTypes.LOGIN_FAILURE)
    // tap(action => console.log(action.payload))
  );

  @Effect({ dispatch: false })
  logOut(): Observable<Action> {
    return this.actions.ofType(fromActions.AuthActionTypes.LOGOUT).pipe(
      tap(action => {
        // console.log('Effect action: ', action);
        // this.router.navigate(['']);
        // this.localStorageService.setItem(AUTH_KEY, { isAuthenticated: false });
      })
    );
  }

  // @Effect()
  // singUp: Observable<any> = this.actions
  //   .ofType(AuthActionTypes.SIGNUP)
  //   .map((action: SignUp) => action.payload)
  //   .switchMap(payload => {
  //     return this.authService
  //       .signUp(payload)
  //       .map(user => {
  //         setTimeout(() => {
  //           this.router.navigate(['']);
  //         }, 3000);
  //         return new SignUpSuccess({ data: user });
  //       })
  //       .catch(err => {
  //         return Observable.of(new SignUpFailure({ error: err }));
  //       });
  //   });
}
