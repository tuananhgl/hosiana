import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-differences',
  templateUrl: './card-differences.component.html',
  styleUrls: ['./card-differences.component.scss']
})
export class CardDifferencesComponent implements OnInit {
  differences = [];
  constructor() {
    this.differences = [
      {
        image: 'assets/images/why1.png',
        content:
          'At Hosiana we believe that real estate business require and should really only be participated in by agencies with strong backgounds who are equipped to handle transactions with utomst skill and diligence, the kind that bolsters customer confidence.'
      },
      {
        image: 'assets/images/why2.png',
        content:
          'Only selected real estage agencies can post listings on Hosiana.vn and communicate through our tools in the interest of quality control and credible listings.'
      },
      {
        image: 'assets/images/why3.png',
        content:
          'As a result, we do not accept listing from individual agents witn nos strong professional aggiliations.'
      },
      {
        image: 'assets/images/why4.png',
        content:
          'Our priority is to be a credible go-to resource in every aspect of your estate journey, every step of the way as we continually offer you updated advertsing and marketing tools.'
      }
    ];
  }

  ngOnInit() {}
}
