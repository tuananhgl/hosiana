import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-card-services',
    templateUrl: './card-services.component.html',
    styleUrls: ['./card-services.component.scss']
})
export class CardServicesComponent implements OnInit {
    @Input()
    color = '';
    @Input()
    title = '';
    @Input()
    elementId = '';
    services = [];
    constructor() {
    }

    ngOnInit() {
        if(this.title == 'REAL ESTATE AGENCY') {
            this.services = [
                {
                    title: 'BASIC',
                    price: {
                        value: '390,000đ/ month',
                        show: 1
                    },
                    sale: 0,
                    oldPrice: '',
                    listContent: [
                        {value: '1 user', show: 1}, 
                        {value: '20 listing/month', show: 1}, 
                        {value: 'No Team Management ', show: 0}, 
                        {value: 'No Review System', show: 0},
                        {value: 'Offer listing: 20 listing/month', show: 1},
                        {value: 'No Listing Assistant', show: 0}
                    ]
                },
                {
                    title: 'PLUS',
                    price: {
                        value: '2,099,000đ/ month',
                        show: 1
                    },
                    oldPrice: '',
                    sale: 50,
                    listContent: [
                        {value: '10 user',show: 1}, 
                        {value: '100 listing/month', show: 1},
                        {value: 'Team Management', show: 1},
                        {value: 'Review System', show: 1},
                        {value: 'Offer listing: 100 listing/month', show: 1},
                        {value: 'No Listing Assistant', show: 0}
                    ]
                },
                  {
                    title: 'PRO',
                    price: {
                        value: 'Custom Price',
                        show: 1
                    },
                    sale: 0,
                    oldPrice: '',
                    listContent: [
                        {value: 'Custom user', show: 1},
                        {value: 'Unlimited Listing/ month', show: 1},
                        {value: 'Team Management', show: 1},
                        {value: 'Review System ', show: 1},
                        {value: 'Offer listing: Unlimited listing/month', show: 1},
                        {value: 'Listing Assistant', show: 1}
                    ]
                }
            ];
        } else if(this.title == 'HOME SERVICE') {
            this.services = [
                {
                    title: 'BASIC',
                    price: {
                        value: 'FREE',
                        show: 1
                    },
                    sale: 0,
                    oldPrice: '',
                    listContent: [
                        {value: 'No user', show: 1}, 
                        {value: 'No Search Priority', show: 1}, 
                        {value: 'No Click to Booking', show: 0}, 
                        {value: 'No Booking Support', show: 0},
                        {value: 'No Review System ', show: 1},
                        {value: 'No Service Ads', show: 0}
                    ]
                },
                {
                    title: 'PLUS',
                    price: {
                        value: '390,000đ/ month',
                        show: 1
                    },
                    oldPrice: '',
                    sale: 30,
                    listContent: [
                        {value: '1 user',show: 1}, 
                        {value: 'Search Priority', show: 1},
                        {value: 'Click to Booking', show: 1},
                        {value: 'No Booking Support', show: 0},
                        {value: 'Review System', show: 1},
                        {value: '8 Service Ads', show: 0}
                    ]
                },
                  {
                    title: 'PRO',
                    price: {
                        value: 'Custom Price',
                        show: 1
                    },
                    sale: 0,
                    oldPrice: '',
                    listContent: [
                        {value: 'Customer user', show: 1},
                        {value: 'Search Priority on Top', show: 1},
                        {value: 'Click To Booking', show: 1},
                        {value: 'Booking Support', show: 1},
                        {value: 'Review System', show: 1},
                        {value: '20 Services Ads and other support advertising', show: 1}
                    ]
                }
            ];
        } else if(this.title == 'PROJECT DEVELOPER') {
            this.services = [
                {
                    title: 'BASIC',
                    price: {
                        value: 'FREE',
                        show: 1
                    },
                    sale: 0,
                    oldPrice: '',
                    listContent: [
                        {value: 'Connect up to 50 users', show: 1}, 
                        {value: 'Basic Ranking', show: 1}, 
                        {value: "Don't know Who's viewed your profile", show: 1}, 
                        {value: 'Receive up to 2 deal/ month', show: 1},
                        {value: 'View Feed ', show: 1},
                        {value: 'No Post Feed', show: 1}
                    ]
                },
                {
                    title: 'PLUS',
                    price: {
                        value: '390,000đ/ month',
                        show: 1
                    },
                    oldPrice: '',
                    sale: 50,
                    listContent: [
                        {value: 'Connect up to 250 users',show: 1}, 
                        {value: 'Plus Ranking', show: 1},
                        {value: "Know Who's view your profile", show: 1},
                        {value: 'Receive up to 30 deals/ month', show: 1},
                        {value: 'View Feed', show: 1},
                        {value: '5 Post Feed', show: 1}
                    ]
                },
                  {
                    title: 'PRO',
                    price: {
                        value: 'Custom Price',
                        show: 1
                    },
                    sale: 0,
                    oldPrice: '',
                    listContent: [
                        {value: 'Connect up to 500+ uses', show: 1},
                        {value: 'Pro Ranking', show: 1},
                        {value: "Know Who's view your profile", show: 1},
                        {value: 'Receive up to 100 deals/ month', show: 1},
                        {value: 'View Feed', show: 1},
                        {value: 'Many Post Feed', show: 1}
                    ]
                }
            ];
        }
    }
}
