import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';

import {
  Agency,
  Category,
  Facility,
  ExchangeRate,
  Item,
  City,
  District,
  Ward,
  GeoCode,
  PromotionList,
  AgencyStaff
} from '../../models/agency';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as fromActions from '../../store/actions';
import * as fromCategoryReducers from '../../store/category.reducers';
import * as fromFacilityReducers from '../../store/facility.reducers';
import * as fromExchangeRateReducers from '../../store/exchange-rate.reducers';
import * as fromProjectReducers from '../../store/project.reducers';
import * as fromCityReducers from '../../store/city.reducers';
import * as fromDistrictReducers from '../../store/district.reducers';
import * as fromWardReducers from '../../store/ward.reducers';
import * as fromGeoCodeReducers from '../../store/geography-code.reducers';
import * as fromPromotionReducers from '../../store/promotion-list.reducers';
import * as fromAgencyStaffReducers from '../../store/agency-staff.reducers';

@Component({
  selector: 'app-add-listing',
  templateUrl: './add-listing.component.html',
  styleUrls: ['./add-listing.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AddListingComponent implements OnInit {
  step: String;
  formValues: FormGroup;
  formComercial: FormGroup;
  formFurnished: FormGroup;
  submitValues: Agency;

  categories: Observable<Category[]>;
  facilities: Observable<Facility[]>;
  exchangeRates: Observable<ExchangeRate>;
  projects: Observable<Item[]>;
  cities: Observable<City[]>;
  districts: Observable<District[]>;
  wards: Observable<Ward[]>;
  geoCode: Observable<GeoCode>;
  promotions: Observable<PromotionList[]>;
  agencyStaffs: Observable<AgencyStaff[]>;

  constructor(private formBuilder: FormBuilder, private store: Store<any>) {}

  ngOnInit() {
    this.store.dispatch(new fromActions.LoadAllCategories());
    this.store.dispatch(new fromActions.LoadAllFacilities());
    this.store.dispatch(new fromActions.LoadExchangeRate());
    this.store.dispatch(new fromActions.LoadProjects());
    this.store.dispatch(new fromActions.LoadCities());
    this.store.dispatch(new fromActions.LoadPromotions());
    this.store.dispatch(new fromActions.LoadAgencyStaffs());

    this.categories = this.store.select(fromCategoryReducers.selectAll);
    this.facilities = this.store.select(fromFacilityReducers.selectAll);
    this.exchangeRates = this.store.select(fromExchangeRateReducers.selectAll);
    this.projects = this.store.select(fromProjectReducers.selectAll);
    this.cities = this.store.select(fromCityReducers.selectAll);
    this.districts = this.store.select(fromDistrictReducers.selectAll);
    this.wards = this.store.select(fromWardReducers.selectAll);
    this.geoCode = this.store.select(fromGeoCodeReducers.selectAll);
    this.promotions = this.store.select(fromPromotionReducers.selectAll);
    this.agencyStaffs = this.store.select(fromAgencyStaffReducers.selectAll);

    this.step = 'tab-details';
    this.initFormValues();
    this.initFormComercial();
    this.initFormFurnised();
  }

  handleChangeTab(e: NgbTabChangeEvent): void {
    this.step = e.nextId;
  }

  handleGoToStep(e: Event, tabSet: any, tabName: string): void {
    e.preventDefault();
    if (this.formValues.valid) {
      this.step = tabName;
      tabSet.select(tabName);
    }
  }

  handlePublish(e: Event): void {
    e.preventDefault();
    const convertSubmitValues = this.convertToSubmitValues();
    this.submitValues = Object.assign({}, this.submitValues, convertSubmitValues);

    console.log(this.submitValues);
    this.store.dispatch(new fromActions.Create({ agency: this.submitValues }));
  }

  handleCancel(e: Event): void {
    e.preventDefault();
  }

  handleSaveAsDraft(e: Event): void {
    e.preventDefault();
  }

  initFormComercial(): void {
    this.formComercial = this.formBuilder.group({
      floors: [0, [Validators.required, Validators.pattern('^[0-9]*$')]],
      bathrooms: [0, [Validators.required, Validators.pattern('^[0-9]*$')]],
      bedrooms: [0, [Validators.required, Validators.pattern('^[0-9]*$')]],
      grossFloorArea: [0, [Validators.required, Validators.pattern('^[0-9]*$')]],
      landArea: [0, [Validators.required, Validators.pattern('^[0-9]*$')]],
      landType: [[]]
    });
  }

  initFormFurnised(): void {
    this.formFurnished = this.formBuilder.group({
      furnished: [0],
      parking: [0],
      petAllowed: [0],
      accessForCar: [0],
      landSharing: [0]
    });
  }

  initFormValues(): void {
    this.formValues = this.formBuilder.group({
      listingType: ['1'],
      serviceId: ['1'],
      cateId: ['1'],
      properties: [[]],
      price: this.formBuilder.group({
        unitCurrency: ['vnd'],
        mainPrice: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
        deposit: ['', Validators.pattern('^[0-9]*$')],
        manageFee: ['', Validators.pattern('^[0-9]*$')],
        rentalNext: ['', Validators.pattern('^[0-9]*$')],
        includeFee: [0],
        loanSupport: [0]
      }),
      location: this.formBuilder.group({
        projectId: [null],
        projectName: [''],
        address: this.formBuilder.group({
          cityId: [{value: null, disabled: false}, Validators.required],
          districtId: [{value: null, disabled: false}, Validators.required],
          wardId: [{value: null, disabled: false}, Validators.required],
          address: [{value: '', disabled: false}, Validators.required],
          latitude: [{value: '', disabled: false}, Validators.required],
          longitude: [{value: '', disabled: false}, Validators.required]
        })
      }),
      facilities: [],
      desc: this.formBuilder.group({
        title: this.formBuilder.group({
          vi: ['', Validators.required],
          en: ['', Validators.required]
        }),
        description: this.formBuilder.group({
          vi: ['', Validators.required],
          en: ['', Validators.required]
        })
      }),
      medias: this.formBuilder.group({
        photos: [[]],
        link: ['']
      }),
      swap: [],
      contacts: this.formBuilder.group({
        agents: [[]],
        contact: this.formBuilder.group({
          name: [''],
          phone: [''],
          email: [''],
          comNumber: [0],
          comPercent: [0]
        })
      }),
      emailContact: [''],
      isRenew: [''],
      promotions: this.formBuilder.group({
        numberDays: [7],
        publishedAt: [''],
        packages: [],
        isRenew: ['0']
      })
    });
  }

  convertToSubmitValues(): Agency {
    const properties = [];
    const formValues = this.formValues.value;
    Object.keys(this.formComercial.value).forEach(key => {
      properties.push({
        key,
        value: this.formComercial.value[key]
      });
    });
    Object.keys(this.formFurnished.value).forEach(key => {
      properties.push({
        key,
        value: this.formFurnished.value[key]
      });
    });
    return {
      listingType: formValues.listingType * 1 || 1,
      serviceId: formValues.serviceId * 1 || 1,
      cateId: formValues.cateId * 1 || 1,
      properties: properties,
      price: {
        unitCurrency: formValues.price.unitCurrency || '',
        mainPrice: formValues.price.mainPrice || 0,
        deposit: formValues.price.deposit || 0,
        manageFee: formValues.price.manageFee || 0,
        rentalNext: formValues.price.rentalNext || 0,
        includeFee: formValues.price.includeFee || false,
        loanSupport: formValues.price.loanSupport || false
      },
      location: {
        projectId: formValues.location.projectId || '',
        projectName: formValues.location.projectName || '',
        address: {
          cityId: formValues.location.address.cityId || 1,
          districtId: formValues.location.address.districtId || 1,
          wardId: formValues.location.address.wardId || 1,
          address: formValues.location.address.address || '',
          latitude: formValues.location.address.latitude || '',
          longitude: formValues.location.address.longitude || ''
        }
      },
      facilities: formValues.facilities || [],
      desc: {
        title: {
          vi: formValues.desc.title.vi || '',
          en: formValues.desc.title.en || ''
        },
        description: {
          vi: formValues.desc.description.vi || '',
          en: formValues.desc.description.en || ''
        }
      },
      medias: {
        photos: formValues.medias.photos || [],
        link: formValues.medias.link || ''
      },
      swap: formValues.swap || [],
      contacts: {
        agents: formValues.contacts.agents || [],
        contact: {
          name: formValues.contacts.contact.name || '',
          phone: formValues.contacts.contact.phone || '',
          email: formValues.contacts.contact.email || '',
          comNumber: formValues.contacts.contact.comNumber || 0,
          comPercent: formValues.contacts.contact.comPercent || 0
        }
      },
      emailContact: 'thuanngo@abc.xyz',
      promotions: {
        numberDays: formValues.promotions.numberDays || 0,
        publishedAt: formValues.promotions.publishedAt || '2018-08-09 15:51:33',
        packages: formValues.promotions.packages || [],
        isRenew: formValues.promotions.isRenew || false
      },
      isRenew: formValues.promotions.isRenew || false,
      publish: true,
      publishedAt: formValues.promotions.publishedAt || '2018-08-09 15:51:33'
    };
  }
}
