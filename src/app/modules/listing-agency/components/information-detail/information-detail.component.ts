import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AgencyService } from '@app/modules/listing-agency/services/agency.service';

@Component({
    selector: 'app-information-detail',
    templateUrl: './information-detail.component.html',
    styleUrls: ['./information-detail.component.scss']
})
export class InformationDetailComponent implements OnInit {
    @Input() formValues: FormGroup;
    @Input() formComercial: FormGroup;
    @Input() formFurnished: FormGroup;
    @Input() propertyTypeList;
    listingType: Array<any>;
    serviceType: Array<any>;

    landtypes: Array<object> = [];
    listLandType: Array<string> = ['Industrial land', 'Commercial land', 'Residential land', 'ABC Bakery', 'Lotte'];

    ngOnInit() {
        this.listingType = AgencyService.getListingType();
        this.serviceType = AgencyService.getServiceType();
        this.listLandType.forEach((c, i) => {
            this.landtypes.push({ id: i + 1, title: c });
        });
    }
}
