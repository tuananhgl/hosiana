import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-preview-promote',
  templateUrl: './preview-promote.component.html',
  styleUrls: ['./preview-promote.component.scss']
})
export class PreviewPromoteComponent implements OnInit {
  @Input() formValues: FormGroup;
  @Input() promotions: any;
  packagesChosen = [];
  previewItem: any;

  ngOnInit() {
    this.promotions.subscribe(res => console.log('res: ', res));

    this.previewItem = {
      isNew: true,
      isHavingGift: true,
      type: 'sale',
      image:
        'https://res.cloudinary.com/hosiana/image/upload/f_auto,q_auto/w_836,h_537/production/assets/listing/HOSIANA-L0011398-0.jpg',
      title: 'Căn hộ 3 phòng ngủ cho thuê tại Vinhomes 54 Nguyễn Chí Thanh',
      location: 'Hà Nội, Đống Đa',
      price: '400 Triệu'
    };
  }

  handleChoosePackage(event): void {
    if (event.target.checked) {
      this.packagesChosen.push(event.target.value * 1);
    } else {
      const index = this.packagesChosen.indexOf(event.target.value * 1);
      this.packagesChosen.splice(index, 1);
    }
    console.log(event.target.value);
    this.formValues.patchValue({ promotions: { packages: this.packagesChosen } });
  }
}
