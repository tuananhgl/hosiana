import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-contact-detail',
    templateUrl: './contact-detail.component.html',
    styleUrls: ['./contact-detail.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ContactDetailComponent implements OnInit {
    @Input() formValues: FormGroup;
    @Input() agencyStaffs: any;

    options: FormGroup;
    listContact: Array<any>;
    listTeamContact: Array<any>;
    listContactSelect = [];
    listTeamContactSelect = [];

    showFormAddContactmanual = false;

    ngOnInit() {
        this.agencyStaffs.subscribe(data => {
            if (data) {
                this.listContact = data.map(item => {
                    item.commission = 0;
                    item.commissionCurency = 'number';
                    item.choosen = false;
                    return item;
                });
            }
        });

        this.listTeamContact = [
            {
                id: 1,
                title: 'Other IA',
                total: 5,
                commission: 0,
                commissionCurency: 'number',
                choosen: false
            },
            {
                id: 2,
                title: 'Best independent agents',
                total: 3,
                commission: 0,
                commissionCurency: 'number',
                choosen: false
            }
        ];
    }

    handleShowFormAddContactManual(): void {
        this.showFormAddContactmanual = true;
    }

    handleChooseAgent(event, type): void {
        if (event.target.checked) {
            if (type === 'agent') {
                const commission = this.listContact.find(item => item.id === event.target.value * 1);
                this.listContactSelect.push({
                    userId: event.target.value * 1,
                    groupId: 0,
                    comNumber: commission.commissionCurency === 'number' ? commission.commission : 0,
                    comPercent: commission.commissionCurency === '%' ? commission.commission : 0,
                });
                this.listContact.forEach(item => {
                    if (item.id === event.target.value * 1) {
                        item.choosen = true;
                    }
                    return item;
                });
            } else {
                const commission = this.listTeamContact.find(item => item.id === event.target.value * 1);
                this.listTeamContactSelect.push({
                    userId: 0,
                    groupId: event.target.value * 1,
                    comNumber: commission.commissionCurency === 'number' ? commission.commission : 0,
                    comPercent: commission.commissionCurency === '%' ? commission.commission : 0,
                });
                this.listTeamContact.forEach(item => {
                    if (item.id === event.target.value * 1) {
                        item.choosen = true;
                    }
                    return item;
                });
            }
        } else {
            if (type === 'agent') {
                this.listContactSelect = this.listContactSelect.filter(item => item.userId !== event.target.value * 1);
                this.listContact.forEach(item => {
                    if (item.id === event.target.value * 1) {
                        item.choosen = false;
                    }
                    return item;
                });
            } else {
                this.listTeamContactSelect = this.listTeamContactSelect.filter(item => item.userId !== event.target.value * 1);
            }
            this.listTeamContact.forEach(item => {
                if (item.id === event.target.value * 1) {
                    item.choosen = false;
                }
                return item;
            });
        }
        this.formValues.patchValue({ contacts: { agents: type === 'agent' ? this.listContactSelect : this.listTeamContactSelect } });
    }
}
