import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
@Component({
    selector: 'app-property-detail',
    templateUrl: './property-detail.component.html',
    styleUrls: ['./property-detail.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PropertyDetailComponent implements OnInit {
    @Input()
    formValues: FormGroup;
    images = [];
    multi = true;

    ngOnInit() {
        // console.log('formValues: ', this.formValues.controls.desc.controls.description.controls.en.invalid);

        this.images = [
            'https://res.cloudinary.com/hosiana/image/upload/f_auto,q_auto/w_324,h_216/production/assets/listing/HOSIANA-L0012440-0.jpg',
            'https://res.cloudinary.com/hosiana/image/upload/f_auto,q_auto/w_324,h_216/production/assets/listing/HOSIANA-L0012441-0.jpg',
            'https://res.cloudinary.com/hosiana/image/upload/f_auto,q_auto/w_324,h_216/production/assets/listing/HOSIANA-L0012592-0.jpg'
        ];
    }

    handleUploadSuccess(photos) {
        // this.formValues.value.medias.photos = this.formValues.value.medias.photos.concat(photos);
        // this.formValues.value.medias.photos = [...this.formValues.value.medias.photos, ...photos]; // not work
        this.formValues.value.medias.photos.push(...photos);
    }

}
