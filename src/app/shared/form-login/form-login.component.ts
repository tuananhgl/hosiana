import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { User } from '@core/models/user';
import { AppState, selectAuthState } from '@core/store/app.states';
import { LogIn, LogOut } from '@core/store/auth/actions';

@Component({
  selector: 'app-form-login',
  templateUrl: './form-login.component.html',
  styleUrls: ['./form-login.component.scss']
})
export class FormLoginComponent implements OnInit {
  loginForm: FormGroup;
  getState: Observable<any>;
  errorMessage: string | null;
  windowDefined;
  public size:any = 'nomal';
  public lang:any = 'en';
  public theme:any = 'Light';
  public type:any = 'type';
  public captchaIsLoaded = false;
  public captchaSuccess = false;
  public captchaResponse?: string;

  constructor(private store: Store<AppState>, private formBuilder: FormBuilder) {
    this.getState = this.store.select(selectAuthState);
  }

  ngOnInit() {
    if (typeof window !== 'undefined') {
      this.windowDefined = true;
    }
    this.getState.subscribe(state => {
      // console.log(state.errorMessage);
      this.errorMessage = state.errorMessage;
    });

    this.loginForm = this.formBuilder.group({
      // dev.hos@yopmail.com
      email: ['agency.dev@yopmail.com', { validators: [Validators.required, Validators.minLength(6), Validators.email], updateOn: 'blur' }],
      password: ['123456', { validators: [Validators.required, Validators.minLength(6)], updateOn: 'blur' }]
    });
  }

  handleLoad(): void {
      this.captchaIsLoaded = true;
  }

  handleSuccess(captchaResponse: string): void {
      this.captchaSuccess = true;
  }
  onSubmit(): void {
    if (this.loginForm.invalid || this.captchaSuccess === false) {
      return;
  }
    // const payload = {
    //     email: this.loginForm.value.email,
    //     password: this.loginForm.password
    // };

    const payload: User = this.loginForm.value;
    this.store.dispatch(new LogIn(payload));
  }
  logOut(): void {
    this.store.dispatch(new LogOut());
  }
}
